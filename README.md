#Links For Critical Thinkers

#Index


Rules when editing this file:

1. You can add new topics by adding <code>##Title</code>
2. You should keep things in **alphabetical order.**
3. The in-line format for adding a video is ```- [Video-title](yt-url) / <code>video-length</code> ```
4. You may add Youtube channels as well under this format ```- [yt-Channel](yt-url)```

## Articles
- [Cultural Marxism](http://en.metapedia.org/wiki/Cultural_Marxism)

## Audio Books
- [Universally Preferable Behaviour: A Rational Proof of Secular Ethics (UPB) / 5:48:28](https://www.youtube.com/watch?v=vZvTXFxPwb0&)

## Comedy Hour Features
- [Bill Maher - I'm Swiss (2005) / 1:16:46](https://www.youtube.com/watch?v=cntgq0jEkJE)

## Interviews
- [Penn Jillette on Capitalism, Magic and Morality](https://www.youtube.com/watch?v=3nOgdc5JmYM)

## Red Pill Philosophy - Men Going Their Own Way
- [SandmanMGTOW](https://www.youtube.com/user/SandmanMGTOW)
- [The Truth About Robin Williams](https://www.youtube.com/watch?v=diyuAXzN7yo)

## Talks
- [Rethinking Learning with Salman Khan ](https://www.youtube.com/watch?v=W-vj6BhQa5w)
- [25 Most Difficult Negotiation Tactics](https://www.youtube.com/watch?v=wI-p2eDVj4k)
- [Zach Holman Talks](http://zachholman.com/talks)
