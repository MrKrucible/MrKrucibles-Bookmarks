#Youtube Collection For American Intellectuals
Rules when editing this file:

1. You can add new topics by adding <code>##Title</code>
2. You should keep things in **alphabetical order.**
3. The usual format for videos is ```Video-title / <code>video-length</code> ```

##Audio Books
- [Universally Preferable Behaviour: A Rational Proof of Secular Ethics (UPB) / 5:48:28](https://www.youtube.com/watch?v=vZvTXFxPwb0&)

##Comedy Hour Features
- [Bill Maher - I'm Swiss (2005) / 1:16:46](https://www.youtube.com/watch?v=cntgq0jEkJE)

##Red Pill Philosophy
- [Red Pill Rage - MGTOW / 10:00](https://www.youtube.com/watch?v=mwXRi5cLvts)
